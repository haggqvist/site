# Personal Website

[![Netlify Status](https://api.netlify.com/api/v1/badges/f981174d-c184-4d0a-905c-5ab9dc549619/deploy-status)](https://app.netlify.com/sites/haggqvist/deploys)

This project is my personal website.

## Backlog

This is just a list of things that are pending:

* Create a suitable `docker` setup for development
