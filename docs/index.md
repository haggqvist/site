# Home

Random technology-related stuff I have written down over the years in the pursuit of improving my understanding of things and organizing information.

While the intended audience is my future self, others may find value in this information (unlikely).
