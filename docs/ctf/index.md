# Capture the Flag

These pages contain notes and solutions for "Capture the Flag" games I've
attempted.

## Exploit Exercises

[Exploit Exercises][ee] is a great source for
security challenges. To date, I've had a try at the following images:

|Image|Status|
|---|:---:|
|[Nebula](./nebula/index.md)|🟢|
|[Protostar](./protostar/index.md)|🟢|

## Over The Wire

Decided to try out some exercises from [Over The Wire][otw].

|Game|Status|
|---|:---:|
|[Natas](./natas/index.md)|🟢|
|Bandit (no write-up, see [repo])|🟢|

[ee]: https://web.archive.org/web/20201223182205/https://exploit-exercises.lains.space/
[otw]: https://overthewire.org/wargames/
[repo]: https://gitlab.com/haggqvist/otw/
