# bash

Some notes on [`bash`](https://www.gnu.org/software/bash/).

## Ignore Host Keys

Sometimes it can be useful to ignore host key verification when using `ssh`. For example, you may be connecting to VMs that generate new keys on startup, or occassionally re-use the same IP addresses (when connecting via IP rather than hostname). The following `alias` can be used as a quick workaround:

``` bash
alias ssh-ignore='ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no"'
$ ssh-ignore user@192.168.99.101
```

## Wake on LAN

Why bother getting up to power on the file server?

??? example "wake.sh"
    ``` bash
    mac=$1
    mac=$(echo $mac |sed 's/[: -]//g')
    broadcast=255.255.255.255
    port=9

    # FF FF FF FF FF FF
    # MAC * 16
    packet=$(
        printf "F%.0s" {1..12}
        printf "$mac%.0s" {1..16}
    )

    # hex encode
    packet=$(echo $packet |sed -e 's/../\\x&/g')

    # broadcast
    echo -e $packet |nc -w1 -u -b $broadcast $port
    ```

```
echo "12:34:56:78:9A:BC" > ~/fileserver.mac
./wake.sh $(cat fileserver.mac)
```

## Time Zone Conversion

While there are several very useful online tools to convert time stamps, I wanted something that doesn't require an Internet connection and helps quickly convert between some time zones I find myself working with regularly. It's not particularly elegant, but gets the job done.

Sample output:

```
$ ./tz.sh 13:37 CET
Sat Feb 20 13:37 CET
====================
Sat Feb 20 23:37 AEDT
Sat Feb 20 18:07 IST
Sat Feb 20 12:37 UTC
Sat Feb 20 07:37 EST
Sat Feb 20 06:37 CST
Sat Feb 20 04:37 PST
```

??? example "tz.sh"
    ``` bash
    #!/bin/bash

    AEST="Australia/Sydney"
    IST="Asia/Kolkata"
    CET="Europe/Stockholm"
    UTC="UTC"
    EST="America/New_York"
    CST="America/Chicago"
    PST="America/Los_Angeles"

    TZONES="${AEST} ${IST} ${CET} ${UTC} ${EST} ${CST} ${PST}"
    FORMAT="+%a %b %d %H:%M %Z"

    if [ "$1" ] ; then
        TIME=$1
    else
        TIME=$(date "${FORMAT}")
    fi

    case ${2^^} in
        AEST)
            TIME_ZONE=$AEST ;;
        IST)
            TIME_ZONE=$IST ;;
        CET)
            TIME_ZONE=$CET ;;
        EST)
            TIME_ZONE=$EST ;;
        CST)
            TIME_ZONE=$CST ;;
        PST)
            TIME_ZONE=$PST ;;
        *)
            TIME_ZONE=$UTC ;;
    esac

    print_time()
    {
        TIME_IN=$1
        TZ_OUT=$2
        TZ=${TZ_OUT} date "${FORMAT}" --date="${TIME_IN}"
    }

    TIME_IN=$(TZ=${TIME_ZONE} date "${FORMAT}" --date="${TIME}")
    echo $TIME_IN
    printf "=%.0s" {1..20}
    echo

    for zone in $TZONES ; do
        if [[ $zone == $TIME_ZONE ]] ; then
            :
        else
            print_time "${TIME_IN}" $zone
        fi
    done
    ```
