# Raspberry Pi

Tinkering with the [Raspberry Pi](https://www.raspberrypi.org/).

## Automatic Updates

Some time ago, I set up a Raspberry Pi to run [Pi-hole](https://pi-hole.net/) to block ads for all devices on my network (best $40 I've ever spent). I wanted to ensure the Pi-hole software and its rules to be kept up-to-date on an automated basis. Since the machine is not used for any "critical" workloads, I decided to include the distro and other packages in the update script.

### Update Script

A simple `bash` script can be defined to run the update commands:

???+ example "update.sh"
    ``` sh
    #!/bin/sh

    echo $(date +"%Y-%m-%dT%H:%M:%S")

    # packages
    sudo apt-get update && sudo apt-get dist-upgrade -y
    sudo apt-get autoremove -y
    sudo apt-get autoclean -y

    # pi-hole
    sudo /usr/local/bin/pihole -up

    echo "================================="

    ```

Use `chmod` to ensure only `root` can edit the file:

```
$ sudo chmod 755 /usr/local/bin/update.sh
$ ls -l /usr/local/bin/update.sh
-rwxr-xr-x 1 root staff 191 Aug  8 08:28 /usr/local/bin/update.sh
```

### Crontab

The script will be configured to run periodically using `crontab` and a file is created to store output from each execution, in case there is a need to review the output.

```
$ mkdir ~/logs; touch ~/logs/update
$ crontab -e
```

Add a new entry to the crontab file:

<pre><code># Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
#
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
#
# For more information see the manual pages of crontab(5) and cron(8)
#
# m h  dom mon dow   command
<span style="color:var(--md-code-hl-string-color)">0 2 * * 6 sh /usr/local/bin/update.sh >>/home/pi/logs/update</span></code></pre>

```
$ crontab -e
crontab: installing new crontab
```

The updates will now automatically run every Saturday (day `6` of the week) at 02:00 and the output is logged in `/home/pi/logs/update`.

### Sample Output

??? example "update"
    ```
    2020-08-08T12:51:01
    Get:1 http://archive.raspberrypi.org/debian stretch InRelease [25.4 kB]
    Hit:2 http://raspbian.raspberrypi.org/raspbian stretch InRelease
    Fetched 25.4 kB in 0s (27.1 kB/s)
    Reading package lists...
    Reading package lists...
    Building dependency tree...
    Reading state information...
    Calculating upgrade...
    0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
    Reading package lists...
    Building dependency tree...
    Reading state information...
    0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
    Reading package lists...
    Building dependency tree...
    Reading state information...
      [i] Checking for updates...
      [i] Pi-hole Core:     up to date
      [i] Web Interface:    up to date
      [i] FTL:              up to date

      [✓] Everything is up to date!
    =================================
    ```
