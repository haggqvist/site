# Docker

Notes on building, managing and running Docker containers.

## Minimal Image for HTTP Testing

The other day I was working on a project to automate provisioning of infrastructure to host container applications. As part of this effort, I wanted to have a minimal container image without external dependencies (e.g. a database connection), to test the deployment process and infrastructure.

To achieve this, I created the following Dockerfile which exposes a very basic web service on `PORT`. While this is a completely non-interesting image, it may be useful for testing.

???+ example "Dockerfile"
    ``` docker
    FROM busybox:latest
    ENV PORT 8080
    EXPOSE ${PORT}
    CMD echo "Fish go moo" > index.html; httpd -f -p ${PORT}
    ```

### Usage

#### Build

```
% docker build -t example .
Sending build context to Docker daemon  2.048kB
Step 1/4 : FROM busybox:latest
latest: Pulling from library/busybox
61c5ed1cbdf8: Pull complete 
Digest: sha256:4f47c01fa91355af2865ac10fef5bf6ec9c7f42ad2321377c21e844427972977
Status: Downloaded newer image for busybox:latest
 ---> 018c9d7b792b
Step 2/4 : ENV PORT 8080
 ---> Running in f10f13b46cde
Removing intermediate container f10f13b46cde
 ---> 2fe842562e7a
Step 3/4 : EXPOSE ${PORT}
 ---> Running in fd413d604f9b
Removing intermediate container fd413d604f9b
 ---> 73125953451d
Step 4/4 : CMD echo "Fish go moo" > index.html; httpd -f -p ${PORT}
 ---> Running in a7f467f0703d
Removing intermediate container a7f467f0703d
 ---> eaafb3bad78b
Successfully built eaafb3bad78b
Successfully tagged example:latest
```

#### Run and Test

```
% docker run -d -p 8080:8080 example
...
% curl -i http://127.0.0.1:8080
HTTP/1.0 200 OK
Date: Sun, 02 Aug 2020 18:16:50 GMT
Connection: close
Content-type: text/html
Accept-Ranges: bytes
Last-Modified: Sun, 02 Aug 2020 18:15:34 GMT
Content-Length: 12

Fish go moo
```

#### Tiny!

The resulting image is very small:

```
% docker image ls example --format {{.Size}}
1.22MB
```

## Docker Image from Live ISO

I've been thinking about picking up [Protostar](../ctf/protostar/index.md) again. My previous setup was running the live image using [VirtualBox](https://www.virtualbox.org/), but having recently switched to [WSL2](https://docs.microsoft.com/en-us/windows/wsl/wsl2-index), I can no longer run my VM on some machines. As such, I decided to try and create a Docker image.

???+ warning "Limitations"
    Since a container is not a full Linux system, this approach comes with notable limitations. For example, no programs are launched at startup (unless specified), core dump files will not be created and the network behaves differently. However, many (though not all) exercises can still be performed successfully from the container. If you are interested in attempting Protostar, I recommend to download the ISO image and run it as a virtual machine.

### Prerequisites

First, the [protostar image is downloaded](https://drive.google.com/folderview?id=0B9RbZkKdRR8qbkJjQ2VXbWNlQzg&usp=sharing) into a working directory.


* Confirm the `unsquashfs` tool is available
* Verify the hash to ensure the image was successfully downloaded

```
$ which unsquashfs
/usr/bin/unsquashfs
$ echo "d030796b11e9251f34ee448a95272a4d432cf2ce " $(ls) | shasum -c -
exploit-exercises-protostar-2.iso: OK
```

### Converting the Image

1. Create some working folders:
    ```
    $ mkdir rootfs unsquashfs
    $ ls
    exploit-exercises-protostar-2.iso  rootfs  unsquashfs
    ```
2. Mount the ISO file to the `rootfs` folder as a loop device:
    ```
    $ sudo mount -o loop exploit-exercises-protostar-2.iso rootfs
    mount: /.../protostar/rootfs: WARNING: device write-protected, mounted read-only.
    ```
3. Locate the `filesystem.squashfs` file:
    ```
    $ find . -type f | grep filesystem.squashfs
    ./rootfs/live/filesystem.squashfs
    ```
4. Run `unsquashfs` to extract the file system:
    ```
    $ sudo unsquashfs -f -d unsquashfs/ rootfs/live/filesystem.squashfs
    Parallel unsquashfs: Using 8 processors
    33614 inodes (33227 blocks) to write

    [===============================================================\                ] 26696/33227  80%
    create_inode: socket unsquashfs//var/run/acpid.socket ignored
    [==============================================================================| ] 33226/33227  99%

    created 29619 files
    created 2997 directories
    created 3472 symlinks
    created 0 devices
    created 0 fifos
    ```

    ??? help "Too many levels of symbolic links"
        If you run into this error message, you may be unsquashing the file system into a location that is a symbolic link itself. For example, if running this from WSL you may have a symbolic link for your working folders to ensure your projects stay in sync (I obviously did, and ran into this message). The command also runs very slowly if run from a location on the mounted Windows file system (`/mnt/c/...`).

5. Compress the file system. If needed, copy this file to the host where `docker` is installed:
    ```
    $ sudo tar -C unsquashfs -c . -f protostar.tar
    ```

6. Import the file system to a docker image:
    ```
    $ docker import protostar.tar protostar
    sha256:f43616a6b520b1c11b26da6e5e39d08736959ddf08d1eb531f776eb629846fbf
    $ docker images protostar
    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    protostar           latest              f43616a6b520        16 minutes ago      670MB
    ```

### Running the Image

The image can be started using `docker`:

```
docker run -h protostar -i -t protostar bash
root@protostar:/#
```

And a shell as the user for performing the exercises:

```
docker run --rm -h protostar -it --user 1001:1001 protostar bash
user@protostar:/$ cd opt/protostar/bin/
user@protostar:/opt/protostar/bin$ ./format0 $(python -c 'print "A" * 64 + "\xef\xbe\xad\xde"')
you have hit the target correctly :)
```
