# Visual Studio Code

Some notes on customization of [Visual Studio Code](https://code.visualstudio.com/).

## Terminal Environment

It can be useful to inject additional environment variables into the shell used for development and testing. One such example is to set `PYTHONDONTWRITEBYTECODE` to avoid generation of `.pyc` files and `__pycache__` folders which can add noise to the working environment, but any environment variable can set (or unset using `null`) added this way.

=== "Windows"
    Open `%APPDATA%\Code\User\settings.json` and add the following entry:

    ``` json
    "terminal.integrated.env.windows": {
        "PYTHONDONTWRITEBYTECODE": "1"
    }
    ```

=== "Linux"
    Open `$HOME/.config/Code/User/settings.json` and add the following entry:

    ``` json
    "terminal.integrated.env.linux": {
        "PYTHONDONTWRITEBYTECODE": "1"
    }
    ```

=== "macOS"
    Open `$HOME/Library/Application Support/Code/User/settings.json` and add the following entry:

    ``` json
    "terminal.integrated.env.osx": {
        "PYTHONDONTWRITEBYTECODE": "1"
    }
    ```
