# JavaScript

Misc. JavaScript snippets.

## Checkboxes? Nothx.

It can sometimes be convenient to toggle check boxes off en masse. For example
when websites ask you to accept lots of third party tracking cookies (although
there are arguably better ways of handling this by just browser anti-tracking
configuration). The following snippet can be run in the browser console to
toggle all (visible) checkboxes off for the current site:

```javascript
var checkBoxes = document.querySelectorAll('input[type=checkbox]');
Array.prototype.forEach.call(checkBoxes, function(c, i) { c.checked = false; });
```
