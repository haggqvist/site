# OpenSSL

[OpenSSL][openssl] things.

## Web Server Certificate

### Download Chain

Download the full certificate chain from a web server:

```sh
$ echo |openssl s_client -connect openssl.org:443 -showcerts 2>&1 |
> sed -n "/-BEGIN/,/-END/p" > openssl.org.pem
```

### Print Properties

To only print some properties, the previous command can be piped to the `x509`
module in `openssl`

```sh
$ echo |openssl s_client -connect openssl.org:443 -showcerts 2>&1 |
> sed -n "/-BEGIN/,/-END/p" | openssl x509 -subject -enddate -noout
subject=CN = mta.openssl.org
notAfter=Feb  7 22:14:35 2022 GMT
```

### Default Server Certificate

Not sending an [SNI][sni] can be useful if investigating an issue with the
default certificate returned by a service (e.g. a load balancer) that has
multiple certificates. This is done using the `-noservername` flag:

```sh
$ echo |openssl s_client -connect openssl.org:443 -noservername -showcerts 2>&1 |
> sed -n "/-BEGIN/,/-END/p" | openssl x509 -subject -noout
subject=CN = mta.openssl.org
```

[openssl]: https://www.openssl.org/
[sni]: https://en.wikipedia.org/wiki/Server_Name_Indication
