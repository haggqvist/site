# Advent of Code

Might decide to use this space to explain solutions for [Advent of Code][aoc]
puzzles. In the meantime, my solutions can be found in my [aoc repo].


[aoc]: https://adventofcode.com/
[aoc repo]: https://gitlab.com/haggqvist/aoc
