# Mozilla Firefox

[Firefox][firefox] stuff.

## userContent

Firefox provides a neat feature that enables customization of any site and the
browser UI without installing plugins.

1. Type `about:support` in the Firefox address bar
2. Find the **Profile Folder** item and select **Open Folder**
3. Create a new folder and file at this location: `chrome/userContent.css`
4. Add any styles in `userContent.css` file (example provided below)
5. Back in Firefox, go to `about:config` and ignore the warning. After all, what
could possibly go wrong?! ¯\\_(ツ)_/¯
6. Toggle `toolkit.legacyUserProfileCustomizations.stylesheets` to `true`
7. Restart Firefox
8. ???
9. Profit

Here's an example that places emphasis on the _hacker_ part of
[Hacker News][hn]:

```css title="userContent.css"
@-moz-document domain(ycombinator.com) {

    :root,
    table {
        background-color: #222222 !important;
    }

    *:not(i) {
        font-family: monospace !important;
        color: #BFBFBF !important;
    }

    a:link {
        color: #059F05 !important;
    }

    input,
    form,
    textarea {
        /* we only lurk */
        display: none !important;
    }
}
```

The `!important` rules are needed to make the local stylesheet take precedence.
`#!css @-moz-document domain(...)` is a great way of scoping rules
to a specific site. This is a pretty quick and easy way of customizing the
browsing experience, removing or adjusting elements by simply editing a file.
Another file `userChrome.css` can be used the same way to modify the browser UI.

[firefox]: https://www.mozilla.org/firefox
[hn]: https://news.ycombinator.com/

## Dark Mode PDF Reader

The built-in PDF reader in Firefox ([pdf.js]) does not (as of Firefox 105.0.3)
render in dark mode based on system or browser theme. It also does not provide a
context menu to change this, like the reader view.

Many workarounds proposed regarding this suggest inverting the colour scheme
using `userContent.css`. For example:

```css title="userContent.css"
#viewerContainer > #viewer > .page > .canvasWrapper > canvas {
    filter: grayscale(100%);
    filter: invert(100%);
}
```

A problem with this approach is that it also inverts images, which is often not
desirable. As an alternative, configuring the following settings via
`about:config` can be used to force high-contrast mode and set custom foreground
(text) and background colors:

```
pdfjs.forcePageColors: true
pdfjs.pageColorsBackground: #202020
pdfjs.pageColorsForeground: #DFDFDF
```

A drawback with this config approach is that some PDF images may be displayed
without color.

[pdf.js]: https://github.com/mozilla/pdf.js/
